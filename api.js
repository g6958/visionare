const {Router, query} = require("express");
const visionareUser = require("./models/visionare-users.js")
const artMeta = require("./models/art.js")
const apiRouter = Router();
const path = require("path");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const saltRounds = 10;


apiRouter.get("/user/findOne/", async function (req, res){
	try {
		const user = req.query.username;
		if (!user) {
			return res.status(404).send("User not specified.")
		}
		await visionareUser.findOne(
			{username: req.query.username},
			"username bio pfpSrc").then((foundUser) => {
				const jsonRes = {"UID": foundUser._id, "username":foundUser.username, "bio":foundUser.bio, "pfpSrc": foundUser.pfpSrc}
				res.status(200).send(jsonRes);
			}
		)}
		catch(err) {
			console.error(err);
		}
	}
);

apiRouter.get("/user/findAll/", async function (req, res){
	try {
		await visionareUser.find({}).then((found) => {
			res.status(200).send(found);
		});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.post("/user/sign_up", function(req, res){
	try {
		bcrypt.hash(req.body.pass, saltRounds, function(err, hash){
			if (err) return console.error(err);
			const newUser = new visionareUser({
			fName: req.body.fName,
			lName: req.body.lName,
			username: req.body.username,
			userAge: req.body.age,
			password: hash,
			pfpSrc: __dirname + "/public/asset/userPFP/default.png",
			contributor: false,
				})
			const token = jwt.sign(
				{ 	name: req.body.fName,
					user: req.body.username,
					contributor: false, },
				process.env.TOKEN_KEY,
				{
					expiresIn: "2h",
				}
			);
			newUser.token = token;
			newUser.save(function (err, newUser) {
				if (err) return console.error(err);
				console.log(newUser.username + " saved to database.");
			})
			res.status(201).send({body: token});
		});
		} catch(err) {
		console.error(err);
		res.sendStatus(500);
	}});

apiRouter.post("/user/log_in", async function(req, res){
	try {
		await visionareUser.findOne( 
				{ username: req.body.username },
				"username password fName lName").then((foundUser) => {
					bcrypt.compare(req.body.password, foundUser.password).then((result) => {
						if (result) {
							const token = jwt.sign(
								{
									name: foundUser.fName,
									user: foundUser.username,
									contributor: foundUser.contributor,
									},
								process.env.TOKEN_KEY,
								{
									expiresIn: "2h",
								}
							);
							return res.status(200).send({ body:token, status:200});
						} else {
							console.log("Incorrect username / password combo.")
							res.sendStatus(500);
						}
					})
				})
	} catch(err) {
		console.error(err);
		res.sendStatus(400);
	}
	});

apiRouter.patch("user/saveBio/", async function (req, res){
	try {
		await visionareUser.updateOne({username: req.body.username}, {bio: req.body.bio})
		res.status(200).send({body: "bio updated"});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.patch("/user/updatePfpSrc/", async function (req, res) {
	try {
		console.log(req.body);
		await visionareUser.updateOne({username: req.body.username}, {pfpSrc: req.body.pfpSrc});
		res.sendStatus(200);
	}
	catch(err) {
		console.error(err);
		res.sendStatus(500);
	}
});

apiRouter.patch("/user/pushToArrField/", async function (req, res){
	try {
		const activeUser = req.body.username;
		const field = req.body.field;
		await visionareUser.findOneAndUpdate({username: activeUser},
			 {$push: {[field]: req.body.AID}}).then(() => {
				res.status(200).send({body: `Added ${req.body.AID} to ${activeUser}.${field}`})
			 })
	}catch (err){
		console.error(err);
	}
});

apiRouter.patch("/user/pullFromArrField/", async function (req, res){
	try {
		const activeUser = req.body.username;
		const field = req.body.field;
		await visionareUser.findOneAndUpdate({username: activeUser},
			 {$pull: {[field]: req.body.AID}}).then(() => {
				res.status(200).send({body: `Removed ${req.body.AID} from ${activeUser}.${field}`})
			 })
	}catch (err){
		console.error(err);
	}
});
	

apiRouter.get("/art/findOne", async function(req, res){
	try {
		const title = req.query.title;
		await artMeta.findOne({title: title}, "title author src description")
		.then((foundArt) => {
			const jsonRes = {"AID": foundArt._id, "title":foundArt.title, "author":foundArt.author, "src": foundArt.src, "description": foundArt.description}
			res.status(200).send(jsonRes);
		})
	} catch(err) {
		console.error(err);
		res.sendStatus(600);
	}
});

apiRouter.get("/art/findAll/", async function (req, res){
	try {
		await artMeta.find({}).then((found) => {
			res.status(200).send(found);
		});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.get("/art/findbyArtist/", async function (req, res){
	try {
		const queryArtist = req.query.artist;
		await artMeta.find({author: queryArtist}).then((found) => {
			res.status(200).send(found);
		});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.patch("/art/modifyArtScore", async function(req, res) {
	try{
		const field = req.body.field;
		const amt = req.body.willAdd ? 1 : -1;
		await artMeta.findOneAndUpdate({_id: req.body.AID}, {$inc: {[field]: amt}}).then(() => {
			res.status(200).send({body: `Added ${amt} to ${field} in ${req.body.AID}`});
		})
	} catch(err){
		console.error(err);
		res.sendStatus(500);
	}
})

apiRouter.post("/art/uploadMeta/", async function (req, res) {
	let tmpFile;
	let uploadPath;
	try{
		tmpFile = req.files.givenFile;
		internalPath = __dirname + "/public/pictures/" + tmpFile.name;
		uploadPath = '/visionare/pictures/' + tmpFile.name;
		tmpFile.mv(internalPath, function(err) {
			if (err) {
				console.error(err);
				res.sendStatus(500);
			}
		})

	}catch(err) {
		console.error(err)
		res.sendStatus(500);
	}
	try{
		const newPost = new artMeta({
		title: req.body.title,
		description: req.body.desc,
		src: uploadPath,
		tag1: "misc",
	})
	newPost.save(function (err, newUser) {
		if (err) return console.error(err);
		console.log(newPost.title + " saved to database.");
		res.redirect(`./utility.html?title=${req.body.title}`);
	})} catch(err) {
		console.error(err);
		res.sendStatus(500);
	}
});

apiRouter.patch("/art/saveAuthor/", async function (req, res){
	try {
		console.log(req.body);
		await artMeta.updateOne({title: req.body.title}, {author: req.body.author})
		.then(function() {
			res.status(200).send({body: "Bio Updated"});
		})
	} catch(err) {
		console.error(err);
	}
});



apiRouter.post("/uploadIMG/", async function (req, res) {
	let tmpFile;
	let uploadPath;
	try{
		tmpFile = req.files.givenFile;
		internalPath = __dirname + "/public/pictures/" + tmpFile.name;
		uploadPath = '/visionare/pictures/' + tmpFile.name;
		tmpFile.mv(internalPath, function(err) {
			if (err) {
				console.error(err);
				res.sendStatus(500);
			}
		})
		res.status(200).send({body: uploadPath})
	}catch(err) {
		console.error(err)
		res.sendStatus(500);
	}
})

apiRouter.post("/uploadPFP/", async function (req, res) {
	let tmpFile;
	let uploadPath;
	try{
		tmpFile = req.files.givenFile;
		internalPath = __dirname + "/public/asset/userPFP/" + tmpFile.name;
		uploadPath = '/visionare/pictures/' + tmpFile.name;
		tmpFile.mv(internalPath, function(err) {
			if (err) {
				console.error(err);
				res.sendStatus(500);
			}
		})
		res.redirect(`back`);
		}
	catch(err) {
		console.error(err);
		res.status(500).send("Failed to upload PFP: Internal Error");
	};
})

apiRouter.get("/decodeToken/", async function (req, res){
	try {
		const token = req.body.token || req.query.token || req.headers["x-acess-token"];
		if (!token) {
			return res.status(403).send("A token is required for authentication");
		}
		try {
			const decoded = jwt.verify(token, process.env.TOKEN_KEY);
			return res.status(200).send(decoded);
		} catch(err) {
			return res.status(401).send("Invalid Token");
		}
	} catch(err) {
		console.error(err);
	}
}
);







module.exports = apiRouter;
