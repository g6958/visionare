module.exports = [
  {
    mode: "development",
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      filename: "bundle.js",
    },
  },
];


