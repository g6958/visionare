require("dotenv").config();
const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const api = require ("./api.js");
const port = 8104;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const fileUpload = require('express-fileupload');

app.use(fileUpload());

app.use(
	morgan("dev"),
	bodyParser.json({extended: false}),
	bodyParser.urlencoded({extended: false})
);
app.use(express.static("public"), express.static("dist"));
app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname, "visionare/public/index"));
});
app.use("/api", api);
async function main() {
	mongoose.set("strictQuery", false);
	if (process.env.MODE === "production") {
		await mongoose.connect(`mongodb://192.168.171.67:27017/visionare`, {
			useNewUrlParser: true,
			authSource: "admin",
			user: "visionare",
			pass: process.env.MONGODB_PASS,
		});
	} else {
		await mongoose.connect(`mongodb+srv://visionare.qeai5on.mongodb.net/?retryWrites=true&w=majority`, {
			useNewUrlParser: true,
			authSource: "admin",
			user: "visionare",
			pass: process.env.MONGODB_PASS,
		})
	}
	app.listen(port, () => {
		console.log(`Example app listening on port ${port}`);
	});
}

main().catch((err) => console.error(err));