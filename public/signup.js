const signUpForm = document.forms[0];
const signUpButton = signUpForm[12];


async function userSignUp(form) {  // Function handling the sign up form
    const fNameValue = form.fNameInput.value;
    const lNameValue = form.lNameInput.value;
    const ageValue = form.userAgeInput.value;
    const usernameValue = form.usernameInput.value;
    const passValue = form.passInput.value;
    const cPassValue = form.cPassInput.value;
    if (ageValue >= 13 && confirmPassword(passValue, cPassValue)) {
        let thisToken;
        const response = await fetch(`./api/user/sign_up`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                fName: fNameValue,
                lName: lNameValue,
                username: usernameValue,
                age: ageValue,
                pass: passValue,
            })
        }).then(function(foundUser) {
            return foundUser.json();
        }).then(async function(data) {
            thisToken = data.body;
            sessionStorage.setItem("token", thisToken)});
        await fetch(`./api/decodeToken/?token=${thisToken}`)
        .then(function(foundSession) {
            return foundSession.json();
        }).then(function(dataJSON) {
            window.location.replace(`./userdisplay.html?username=${dataJSON.user}`)
        })
    } else {
        console.log("User is ineligible to sign up at this time.")
    }
}

function confirmPassword(initial, confirm) {
    let match;
    initial === confirm ? match = true : match = false;
    return match;
}





