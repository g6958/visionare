if (sessionStorage.getItem("token")){
    saveAuthorToDB();
} else {
    window.location.replace("index.html");
}

function getQS(lookFor) {
    var query = window.location.search.substring(1);
    var params = query.split('&');
    for (var i=0; i<params.length; i++){
        var pos = params[i].indexOf("=");
        if (pos > 0 && lookFor == params[i].substring(0, pos)) {
            return params[i].substring(pos+1);
        }
    }
    return "";
}


//This is seperate because the upload.html/uploadMeta form submit doesn't prompt for author.
//Band-aid solution, but works. 1.0 drove me crazy
async function saveAuthorToDB() {
    const authorValue = sessionStorage.getItem("user");
    const titleValue = getQS("title");
    await fetch(`./api/art/saveAuthor`, {
        method: "PATCH",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({title: titleValue, author: authorValue})
    }).then(function() {
        sessionStorage.removeItem("user");
        sessionStorage.removeItem("title");
        window.location.replace(`./artdisplay.html?title=${titleValue}`)
    })
}
