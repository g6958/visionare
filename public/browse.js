// VARIABLES AND ARRAYS
const tagButtonArray = Array.from(document.querySelector("#tagList").children)
const activeTagList = document.querySelector("#activeTags");
const activeTags = [];
const imgDisplayContainer = document.querySelector(".artPieces");

const imgArr = [];
gatherImgFromDB();
// Can also be written as--> const imgArr = Array.from(document.querySelectorAll(".galleryImg"));

const searchButton = document.querySelector("#searchIcon");

// Database Functions

async function gatherImgFromDB() {
    await fetch(`./api/art/findAll`)
    .then((data) => data.json())
    .then((JSONdata) => {
        const tmpImgArr = [];
        for (let i = 0; i < JSONdata.length; i++) {
            let current = JSONdata[i];
            const newIMGElement = document.createElement("img");
            newIMGElement.classList.add("galleryImg");
            newIMGElement.src = current.src;
            newIMGElement.alt = current.title;
            newIMGElement.onclick = function () {
                window.location.href = `./artdisplay.html?title=${current.title}`;
            }
            if (current.tag3) {
                newIMGElement.setAttribute("tag3", current.tag3);
            } 
            if (current.tag2) {
                newIMGElement.setAttribute("tag2", current.tag2);
            }
            if (current.tag1) {
                newIMGElement.setAttribute("tag1", current.tag1);
            }
            imgArr.push(newIMGElement);
            imgDisplayContainer.appendChild(newIMGElement);
        }
    })
}



// BUTTON FUNCTIONS
function clickButton(button) {   
    toggleButtonColor(button);
    checkButtonTag(button);
    displayTaggedImages(); 
}

function toggleButtonColor(button) {
    button.classList.toggle("toggled");
}

// TAG FUNCTIONS

function checkButtonTag(button) {
    thisTag = button.getAttribute("id");
    console.log("Clicked tag: " + thisTag);
    button.classList.contains("toggled") ? addTag(thisTag) : removeTag(thisTag);
}

function addTag(tag) {
    let tagLabel = document.createElement("li");
    tagLabel.innerHTML = `${tag}`.toUpperCase();
    tagLabel.setAttribute("id", `${tag}`);
    activeTagList.appendChild(tagLabel);
    activeTags.push(tag);
    console.log(activeTags);
}

function removeTag(tag) {
    let tagLabel = document.querySelector("li#" + `${tag}`);
    let activeList = tagLabel.parentNode;
    activeList.removeChild(tagLabel);
    for (i=0; i<=activeTags.length; i++) {
        if (activeTags[i] == tag) {
            activeTags.splice(i, 1);
        }
    }
    console.log(activeTags);
}

// IMAGE FUNCTIONS

function hideAllImg() {
    for (i=0; i<=imgArr.length-1; i++) {
        console.log("Images hidden");
        imgArr[i].classList.add("hidden");
    }
}

function showAllImg() {
    for (i=0; i<=imgArr.length-1; i++) {
        imgArr[i].classList.remove("hidden");
    }
}

function showImg(img) {
    img.classList.remove("hidden");
}

function hideImg(img) {
    img.classList.add("hidden");
}

function displayTaggedImages() {
    console.log(imgArr);
    for (i=0; i<=imgArr.length; i++) {
        if (activeTags.length == 0) {
            showAllImg();
        }
        else if (activeTags.includes(imgArr[i].getAttribute("tag1"))) {
            console.log(`Image has the tag.`);
            showImg(imgArr[i]);
        }
        else if (activeTags.includes(imgArr[i].getAttribute("tag2"))) {
            console.log(`Image has the tag.`);
            showImg(imgArr[i]);
        }
        else if (activeTags.includes(imgArr[i].getAttribute("tag3"))) {
            console.log(`Image has the tag.`);
            showImg(imgArr[i]);
        }
        else {
            console.log(`Image does not have the tag.`);
            hideImg(imgArr[i]);
        }
    }
}
