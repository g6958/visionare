//DEPRECATED! Where do we use this lol.

const userAuthPortal = document.getElementById("userAuth");

async function getSessionName() {
    const response = await fetch("./api/sessionName").then(res => res.json())
    .then(data => {
        if (data.fName) {
            renameSession(data.fName);
        }
    })
}

function renameSession(name) {
    userAuthPortal.innerText = `Hi, ${name}!`;
    userAuthPortal.href = "./userdisplay.html";
    const logoutA = document.getElementById("logout");
    logoutA.innerText = "Log Out";
    logoutA.onclick = fetch("./api/user/endSession");
}