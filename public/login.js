var userAuthPortal = document.getElementById("userAuth");
const logoutA = document.getElementById("logout");
var currentPage = (location.href.split("/").slice(-1))[0];
var dataUser;


async function userLogin(form) { // Function handling the log in form
    const lUserValue = form.lUserInput.value;
    const lPassValue = form.lPassInput.value;
    await fetch(`./api/user/log_in`, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            username: lUserValue,
            password: lPassValue,
        })
    }).then(function(foundUser) {
            return foundUser.json();
        }).then(async function(data) {
            console.log(data.body);
            sessionStorage.setItem("token", data.body);
            await fetch(`./api/decodeToken/?token=${data.body}`)
            .then(function(foundSession) {
                return foundSession.json();
            }).then(function(dataJSON) {
                window.location.replace(`./userdisplay.html?username=${dataJSON.user}`)
            })
            
        })
    }

async function getSessionName() {
    token = sessionStorage.getItem("token");
    if (token) {
        await fetch(`./api/decodeToken/?token=${token}`)
        .then((response) => response.json())
        .then((response) => {
        if (response.iat) {
            renameSession(response);
            }
        else {
            console.log("JWT expired!");
            terminateSession();
        }}
    )}
    else {
        return false;
    }
}

function renameSession(jsonData) {
    if (jsonData.name) {
        userAuthPortal.innerText = `Hi, ${jsonData.name}!`;
        userAuthPortal.classList.add("noRecolor");
        logoutA.classList.add("noScaleUp");
        sessionStorage.setItem("user", jsonData.user);
        userAuthPortal.href = `userdisplay.html`;
        logoutA.innerText = "Log Out";
        logoutA.onclick = terminateSession;
        }
    }

function terminateSession() {
    sessionStorage.removeItem("token");
    userAuthPortal.innerText = `Sign Up/Log In`;
    userAuthPortal.href = "./signup.html";
    logoutA.innerText = "";
    logoutA.onclick = "";
    location.reload();
}