const uploadFormElement = document.getElementById("userForm");
const loggedOutMessage = document.getElementById("loggedOutMSG");
let token = sessionStorage.getItem("token");
const currentUser = async function() {
    await fetch(`./api/decodeToken/?token=${token}`)
    .then(function(userData) {
        return userData.json();
    }).then(function(data) {
        return data;
    })
}


if (currentUser) {
    uploadFormElement.classList.remove("hidden");
    loggedOutMessage.classList.add("hidden");
} else {
    uploadFormElement.classList.add("hidden");
    loggedOutMessage.classList.remove("hidden");

}

async function uploadForm(form) {
    const titleValue = uploadFormElement.children[1].value;
    const descValue = uploadFormElement.children[3].value;
    sessionStorage.setItem("title", titleValue);
    const artPicture = document.getElementById("imageField").files[0];
    await fetch(`./api/uploadIMG/`, {
        method: "POST",
        enctype: "multipart/form-data",
        headers: {"Content-Type": "application/json"},
        files: artPicture,
        })
        .then(function(res) {
            return res.json()
        })
        .then(async function(resJSON) {
            const path = resJSON.body;
            const user = getSessionName();
            await fetch(`./api/art/uploadMeta/`, {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: {
                    title: titleValue,
                    author: user,
                    description: descValue,
                    src: path,
                }}
            )
        })
    }
