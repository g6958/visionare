const titleContainer = document.getElementById("title");
const authorContainer = document.getElementById("author");
const descContainer = document.getElementById("bioText");
const artDisplayContainer = document.getElementById("art");
const url = location.href;
const ABSPATH = url.substring(0, url.lastIndexOf("?"));
const ASSET = ABSPATH.substring(0, ABSPATH.lastIndexOf("/")) + "/asset/";
const socialSection = document.getElementById("social");
let loadedArtID = "";

function getQS(lookFor) {
    var query = window.location.search.substring(1);
    var params = query.split('&');
    for (var i=0; i<params.length; i++){
        var pos = params[i].indexOf("=");
        if (pos > 0 && lookFor == params[i].substring(0, pos)) {
            return params[i].substring(pos+1);
        }
    }
    return "";
}

async function getArtInfo() {
    var qs = getQS("title");
    await fetch(`./api/art/findOne?title=${qs}`)
        .then(function(artData) {
            return artData.json();
        }).then(function(artDataJSON) {
            setArtPageInfo(artDataJSON);
        })
    };

async function setArtPageInfo(artInfo) {
    loadedArtID = artInfo.AID;
    titleContainer.innerText = artInfo.title;
    authorContainer.innerText = artInfo.author;
    descContainer.innerText = artInfo.description;
    artDisplayContainer.src = artInfo.src;
    console.log(loadedArtID);
}

function drawCommentPrompt() {
    if (!sessionStorage.token) return;
}

function toggleSocialButton(buttonName) {
    const icon = socialSection.querySelectorAll(`[name=${buttonName}]`)[0];
    if (icon.src == ASSET + buttonName + "-empty.png") {
        icon.filled = true;
        icon.src = ASSET + buttonName + "-full.png"
    } else {
        icon.filled = false;
        icon.src = ASSET + buttonName + "-empty.png"
    }
    socialButtonAction(icon);
}

function socialButtonAction(icon) {
    iconStatus = icon.filled ? "active." : "inactive."
    switch (icon.name) {
        case "like":
            //Like button logic goes here.
            console.log("Like button " + iconStatus);
            break;
        case "fav":
            //Favorite button logic goes here.
            console.log("Favorite button " + iconStatus);
            break;
        case "follow":
            //Follow button logic goes here.
            console.log("Follow button " + iconStatus);
            break;
    }
}



