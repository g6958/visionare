const mongoose = require("mongoose");

const visionareUsersSchema = new mongoose.Schema({
    fName: String,
    lName: String,
    userAge: Number,
    username: String,
    password: String,
    contributor: Boolean,
    token: String,
    bio: String,
    pfpSrc: String,
    likedPosts: [String],
    favoritePosts: [String],
});

const visonareUser = mongoose.model("visionareUsers", visionareUsersSchema);
module.exports = visonareUser;