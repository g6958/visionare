# Visionaré 2.0 - Share Your Vision Update

Art Gallarie and Visual Experience Presented Proudly by Xiaohong and Da Bois

Week 1 - Picking up where we left off

     Begin redesigning webpages and web elements for our more modern UI

     Begin implementation of rate, comment, and favorite functionality on posts
     
     Rewrite complex pages (post page, user page) using LIT to condense our logic into self-contained elements
     
     Test fire 2.0 on the server

Week 2 - Continuing Early Development

     Finalize post interactions system

     Continue redesign across pages

     Implement follow system for general users to follow contributor accounts

     Begin work on UI animations

     Begin work on feed algorithm once following and rating systems are a go

Week 3 - The Hump

     Continue development on feed algorithm

     Beautify feed

     Finish with redesigned user customization page

     Assess progress thus far to determine if stretch goals can be completed within timeframe

Week 4 - The Pretty Part

     Basic page redesigns should be done

     Continue making the feed look its best

     Work on stretch goals

     Begin Test Phase

Week 5 - Test Phase

     Observe / create traffic on the site to ensure everything works as intended

     Solve problems as they arise

     else TBD

Week 6 - Capstone for the Capstone

     Last minute changes and fixes

     Test, test, test again
     
     Rehearse final presentation
